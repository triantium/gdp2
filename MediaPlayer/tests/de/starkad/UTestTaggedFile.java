/*
 * Projekt für das Praktikum GdP2
 * Mediaplayer
  * Erstellt von Manuel Müller
 */
package de.starkad;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Müller Manuel
 */
public class UTestTaggedFile extends TestCase {

    private List<String> correctFileNames;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        correctFileNames = new ArrayList();
        File audioPath = new File("./audiofiles");
        File files[] = audioPath.listFiles();
        for (int i = 0; i < files.length; i++) {
            if ((!files[i].getAbsolutePath().contains("cut"))&&(files[i].getAbsolutePath().endsWith(".mp3")||files[i].getAbsolutePath().endsWith(".ogg"))) {
                correctFileNames.add(files[i].getAbsolutePath());
            }
        }

    }

    @Test
    public void testPlay01() throws Exception {
        correctFileNames.forEach((filename) -> {
            System.out.print(filename);
//            TaggedFile tf = new TaggedFile(filename);
//            tf.play();
//            tf.stop();
        });

    }
}
