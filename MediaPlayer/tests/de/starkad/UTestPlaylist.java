/*
 * Projekt für das Praktikum GdP2
 * Mediaplayer
  * Erstellt von Manuel Müller
 */
package de.starkad;

import junit.framework.TestCase;

/**
 *
 * @author Müller Manuel
 */
public class UTestPlaylist extends TestCase {
    
    public UTestPlaylist(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    // TODO add test methods here. The name must begin with 'test'. For example:
    // public void testHello() {}
}
