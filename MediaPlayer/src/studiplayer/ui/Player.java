/*
 * Projekt für das Praktikum GdP2
 * Mediaplayer
  * Erstellt von Manuel Müller
 */
package studiplayer.ui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import studiplayer.audio.AudioFile;
import studiplayer.audio.NotPlayableException;
import studiplayer.audio.PlayList;

/**
 * @author Müller Manuel
 */
public class Player extends JFrame implements ActionListener {

    public final static String EMPTYTITLE = "no current Song";
    public final static String EMPTYDURATION = "--:--";
    public final static String EMPTYPLAYLIST = "empty play list";
    
    public final static String SEPCHAR=File.separator;
    
    //only necessary if testing on Windows
//    public final static String SEPCHAR="/";

    public final static String DEFAULT_PLAYLIST = "playlists" + SEPCHAR
            + "DefaultPlayList.m3u";

    public final static String STARTDURATION = "00:00";
    public final static String CURRENTSONGPREFIX = "Current Song";

    private PlayList playList;
    private PlayListEditor playListEditor;

    private JLabel songDescription;
    private JLabel playTime;
    private volatile boolean stopped;

    private boolean editorVisible;

    JButton playButton;
    JButton pauseButton;
    JButton stopButton;

    public Player(PlayList pl) {

        this.playList = pl;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } catch (InstantiationException ex) {
            ex.printStackTrace();
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        } catch (UnsupportedLookAndFeelException ex) {
            ex.printStackTrace();
        }

        //GUI Creation
        JPanel buttonPanel = new JPanel();

        List<JButton> buttonlist = new ArrayList();

        //TODO icons
        playButton = new JButton(new ImageIcon("icons/play.png"));
        playButton.setActionCommand(Action.AC_PLAY.name());
        playButton.addActionListener(this);
        buttonlist.add(playButton);
        pauseButton = new JButton(new ImageIcon("icons/pause.png"));
        pauseButton.setActionCommand(Action.AC_PAUSE.name());
        pauseButton.addActionListener(this);
        buttonlist.add(pauseButton);
        stopButton = new JButton(new ImageIcon("icons/stop.png"));
        stopButton.setActionCommand(Action.AC_STOP.name());
        stopButton.addActionListener(this);
        buttonlist.add(stopButton);
        JButton nextButton = new JButton(new ImageIcon("icons/next.png"));
        nextButton.setActionCommand(Action.AC_NEXT.name());
        nextButton.addActionListener(this);
        buttonlist.add(nextButton);
        JButton editButton = new JButton(new ImageIcon("icons/pl_editor.png"));
        editButton.setActionCommand(Action.AC_EDIT.name());
        editButton.addActionListener(this);
        buttonlist.add(editButton);

        //just to do some lambda action
        buttonlist.forEach((button) -> buttonPanel.add(button));

        songDescription = new JLabel();
        playTime = new JLabel();

        this.add(songDescription, BorderLayout.NORTH);
        this.add(playTime, BorderLayout.WEST);
        this.add(buttonPanel, BorderLayout.EAST);

        //Call it after Elements have been init
        updateSongInfo(playList.getCurrentAudioFile());
        setButtonState(true);
        pauseButton.setEnabled(false);

        playListEditor = new PlayListEditor(this, this.playList);
        editorVisible = false;
        stopped = true;
        //Activate GUI
        this.pack();
        this.setVisible(true);

    }

    private void updateSongInfo(AudioFile af) {
        Optional<AudioFile> optaf = Optional.ofNullable(af);
        if (optaf.isPresent()) {
            setSongLabel(optaf.get());
        } else {
            System.out.println("Nullreferenz im AudioFile");
            setEmptyLabel();
        }
    }

    private void setSongLabel(AudioFile audiofile) {
        songDescription.setText(audiofile.toString());
        this.setTitle(audiofile.toString());
        playTime.setText(STARTDURATION);
    }

    private void setEmptyLabel() {
        songDescription.setText(EMPTYTITLE);
        playTime.setText(EMPTYDURATION);
        this.setTitle("Stupidplayer".concat(" ").concat(EMPTYPLAYLIST));
    }

    private void playCurrentSong() {

        updateSongInfo(playList.getCurrentAudioFile());
        stopped = false;
        if (!playList.isEmpty() && playList.getCurrentAudioFile() != null) {
            (new PlayerThread()).start();
            (new TimerThread()).start();
        }
    }

    private void stopCurrentSong() {
        //setzte stopped sofort auf wahr damit AudioThread richtig reagiert

        stopped = true;
        playList.getCurrentAudioFile().stop();
        updateSongInfo(playList.getCurrentAudioFile());
    }

    private void setButtonState(boolean stopped) {
        playButton.setEnabled(stopped);
        stopButton.setEnabled(!stopped);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        AudioFile af;
        String cmd = e.getActionCommand();

        if (cmd.equals(Action.AC_PLAY.name())) {
            af = playList.getCurrentAudioFile();
            System.out.println("Playing: ".concat(af.toString()));
            setButtonState(false);
            pauseButton.setEnabled(true);
            playCurrentSong();
        } else if (cmd.equals(Action.AC_PAUSE.name())) {
            af = playList.getCurrentAudioFile();
            System.out.println("Pause: ".concat(af.toString()));
            if (!playList.isEmpty()) {
                if (playList.getCurrentAudioFile() != null) {
                    playList.getCurrentAudioFile().togglePause();
                }
            }
        } else if (cmd.equals(Action.AC_NEXT.name())) {
            //simply stop it and see what happens
            if (!stopped) {
                stopCurrentSong();
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
                Logger.getLogger(Player.class.getName()).log(
                        Level.SEVERE, null, ex);
            }
            playList.changeCurrent();
            af = playList.getCurrentAudioFile();
            System.out.println("NEXT: ".concat(af.toString()));
            setButtonState(false);
            pauseButton.setEnabled(true);
            playCurrentSong();
        } else if (cmd.equals(Action.AC_STOP.name())) {

            af = playList.getCurrentAudioFile();
            System.out.println("Stopping: ".concat(af.toString()));
            playTime.setText(EMPTYDURATION);
            setButtonState(true);
            pauseButton.setEnabled(false);
            stopCurrentSong();
        } else if (cmd.equals(Action.AC_EDIT.name())) {
            if (editorVisible) {
                editorVisible = false;
            } else {
                editorVisible = true;
            }
            playListEditor.setVisible(editorVisible);
        }
    }

    //Threads can be done in two Ways, just go and see the Orcale Doc
    private class TimerThread extends Thread {

        @Override
        public void run() {
            while (!stopped) {
                if (!playList.isEmpty()) {
                    playTime.setText(
                            playList.getCurrentAudioFile().
                                    getFormattedPosition());
                    try {
                        sleep(100);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Player.class.getName()).log(
                                Level.SEVERE, null, ex);
                    }
                }
            }
            System.out.println("Stopped TimerThread");
        }

    }

    private class PlayerThread extends Thread {

        @Override
        public void run() {
            while (!stopped) {
                if (!playList.isEmpty()) {
                    try {
                        System.out.println(playList.getCurrent());
                        playList.getCurrentAudioFile().play();
                    } catch (NotPlayableException ex) {
                        ex.printStackTrace();
                    }
                }
                //after file is played
                if (!stopped) {
                    playList.changeCurrent();
                    updateSongInfo(playList.getCurrentAudioFile());
                } else {
                    playList.getCurrentAudioFile().stop();
                }
            }
            System.out.println("PlayerThread ending");
        }
    }

    public static void main(String[] args) {

        PlayList playlist = new PlayList();
        if (args.length > 0) {
            playlist.loadFromM3U(args[0]);
        } else {
            playlist.loadFromM3U(DEFAULT_PLAYLIST);
        }

        Player player = new Player(playlist);
    }

}
