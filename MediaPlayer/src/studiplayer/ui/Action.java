/*
 * Projekt für das Praktikum GdP2
 * Mediaplayer
  * Erstellt von Manuel Müller
 */
package studiplayer.ui;

/**
 *
 * @author Müller Manuel
 */
public enum Action {
    AC_PLAY, AC_PAUSE, AC_NEXT, AC_STOP, AC_EDIT
}
