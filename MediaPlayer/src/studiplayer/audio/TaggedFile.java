package studiplayer.audio;

/*
 * Projekt für das Praktikum GdP2
 * Mediaplayer
 * Erstellt von Manuel Müller
 */
import java.util.Map;
import studiplayer.basic.TagReader;

/**
 * @author Müller Manuel
 */
public class TaggedFile extends SampledFile {

    //add a field to forfill TestRequirements String album ist defined in
    //Audiofile and will stay there
    private String takeYaField = "";

    public TaggedFile(String p) throws NotPlayableException {
        super(p);
        readAndStoreTags(getPathname());
    }

    public void readAndStoreTags(String pathname) throws NotPlayableException {
        if (pathname.isEmpty()) {
            return;
        }
        Map<String, Object> tags = null;
        try {
            tags = TagReader.readTags(pathname);
        } catch (RuntimeException ex) {
            throw new NotPlayableException(getPathname(), ex);
        }
        //unreachable after throw
        if (tags != null) {
            Object object = tags.get("title");
            if (object != null) {
                String tmp = (String) object;
                if (!tmp.isEmpty()) {
                    title = tmp.trim();
                }
            }
            object = tags.get("author");
            if (object != null) {
                String tmp = (String) object;
                if (!tmp.isEmpty()) {
                    author = tmp.trim();
                }
            }
            object = tags.get("album");
            if (object != null) {
                String tmp = (String) object;
                if (!tmp.isEmpty()) {
                    album = tmp.trim();
                }
            }

            object = tags.get("duration");
            if (object != null) {
                Long tmp = (Long) object;
                if (tmp.longValue() != 0) {
                    duration = tmp.longValue();
                }
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        if (!album.isEmpty()) {
            sb.append(TITLESEPERATOR);
            sb.append(album);
        }
        sb.append(TITLESEPERATOR);
        sb.append(getFormattedDuration());

        return sb.toString();
    }

    @Override
    public String[] fields() {
        String field[] = {getAuthor(), getTitle(), getAlbum(),
            getFormattedDuration()};
        return field;
    }

}
