/*
 * Projekt für das Praktikum GdP2
 * Mediaplayer
  * Erstellt von Manuel Müller
 */
package studiplayer.audio;

import java.util.Comparator;

/**
 * @author Müller Manuel
 */
public class AlbumComparator implements Comparator<AudioFile> {

    @Override
    public int compare(AudioFile o1, AudioFile o2) {
        if (o1 == null || o2 == null) {
            throw new NullPointerException();
        } else {
            if (o1 instanceof TaggedFile) {
                o1 = o1;
            } else {
                o1.setAlbum(null);
            }
            if (o2 instanceof TaggedFile) {
                o2 = o2;
            } else {
                o2.setAlbum(null);
            }

        }

        if (o1.getAlbum() == null && o2.getAlbum() == null) {
            //beide gleich
            return 0;
        } else if (o1.getAlbum() == null) {
            return -1;
        } else if (o2.getAlbum() == null) {
            return 1;
        } else {
            return o1.getAlbum().compareTo(o2.getAlbum());
        }

    }

}
