/*
 * Projekt für das Praktikum GdP2
 * Mediaplayer
  * Erstellt von Manuel Müller
 */

package studiplayer.audio;

import java.util.Comparator;

/**
 * @author Müller Manuel 
 */
public class AuthorComparator implements Comparator<AudioFile>{

   @Override
    public int compare(AudioFile o1, AudioFile o2) {
        if(o1==null||o2==null){
            throw new NullPointerException();
        }else if(o1.getAuthor()==null&&o2.getAuthor()==null){
            //beide gleich
            return 0;
        }else if(o1.getAuthor()==null){
            return -1;
        }else if(o2.getAuthor()==null){
            return 1;
        }else{
            return o1.getAuthor().compareTo(o2.getAuthor());
        }
    }

    

}

