package studiplayer.audio;

import studiplayer.basic.BasicPlayer;

/*
 * Projekt für das Praktikum GdP2
 * Mediaplayer
  * Erstellt von Manuel Müller
 */
/**
 * @author Müller Manuel
 */
public abstract class SampledFile extends AudioFile {

    public SampledFile(String p) throws NotPlayableException {
        super(p);
    }

    public SampledFile() {
        super();
    }

    @Override
    public void play() throws NotPlayableException {
        try {
            BasicPlayer.play(getPathname());
        } catch (RuntimeException ex) {
            throw new NotPlayableException(getPathname(), "SampledFile.play()", ex);
        }
    }

    @Override
    public void togglePause() {
        BasicPlayer.togglePause();
    }

    @Override
    public void stop() {
        BasicPlayer.stop();
    }

    @Override
    public String getFormattedDuration() {
        return timeFormatter(getDuration());
    }

    @Override
    public String getFormattedPosition() {
        return timeFormatter(BasicPlayer.getPosition());
    }

    public static String timeFormatter(long microtime) {
        //99min*60s*1000000
        final long TIMEOVERFLOW;
        TIMEOVERFLOW = 5999999999L;
        if (microtime < 0) {
            throw new RuntimeException(
                    "Negative Zeit für TaggedFile.timeformatter()");
        }
        if (microtime > TIMEOVERFLOW) {
            throw new RuntimeException(
                    "Zeit zu lang für TaggedFile.timeformatter()");
        }

        long tmp = microtime / 1000000;

        int minutes = (int) tmp / 60;

        int seconds = (int) (tmp - (minutes * 60));

        StringBuilder sb = new StringBuilder();
        if (minutes < 10) {
            sb.append("0");
        }
        sb.append(Integer.toString(minutes));
        sb.append(":");
        if (seconds < 10) {
            sb.append("0");
        }
        sb.append(Integer.toString(seconds));

        return sb.toString();

    }

}
