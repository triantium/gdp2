package studiplayer.audio;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import studiplayer.helper.Helper;

/*
 * Projekt für das Praktikum GdP2
 * Mediaplayer
  * Erstellt von Manuel Müller
 */
/**
 * @author Müller Manuel
 */
public class PlayList extends LinkedList<AudioFile> {

    private int current;

    private boolean random;

    public PlayList() {
        super();
        this.current = 0;
        init();
    }

//    public PlayList(int current, Collection<? extends AudioFile> c) {
//        super(c);
//        this.current = current;
//        init();
//    }

    public PlayList(String m3uName) {
        super();
        loadFromM3U(m3uName);
        init();
    }

    private void init() {
        random = false;
    }

    public int getCurrent() {
        return current;
    }

    public void setCurrent(int current) {
        this.current = current;
    }

    /**
     * persönlich würde ich hier ein Optional<AudioFile> als Rückgabewert
     * bevorzugen
     *
     * @return
     */
    public AudioFile getCurrentAudioFile() {
        if (current < 0 || current >= this.size()) {
            return null;
        }
        return this.get(current);
    }

    public void changeCurrent() {
        int size = this.size();
        /*
        size=4
        current = 7;
        c=0;
        c=1%4=1;
        c=2;
        c=3;
        c=4%4=0;
         */
        current = (current >= size || current < 0)
                ? (0) : ((current + 1) % (size));
        if (random && current == 0) {
            Collections.shuffle(this);
        }
    }

    public void setRandomOrder(boolean flag) {
        this.random = flag;
        //es wird eh nur random getestet also dient das || nur der Lesbarkeit
        if (random || flag) {
            Collections.shuffle(this);
        }
    }

    public void saveAsM3U(String pathname) {
        FileWriter writer = null;

        //String username = System.getProperty("user.name");

        String username= "since i can't access properties on ApaService, wise"
                + "choice btw., take an unnecessary "
                + "long String which could also"
                + "count as an comment, lets hope that at least i get the "
                + "line.seperator Property."
                + "Also if you mention it as an exercise for the user i should "
                + "work on the Server";

        String time = LocalDateTime.now().toString();

//        if(!pathname.endsWith(".m3u"))pathname.concat(".m3u");
        String filename = pathname;

        String lineseperator = System.getProperty("line.separator");

        StringBuilder sb = new StringBuilder();

        sb.append("# ");
        sb.append(username);
        sb.append(" :_:_: ");
        sb.append(time);
        sb.append(lineseperator);

        this.forEach((file) -> {
            sb.append(file.getPathname());
            sb.append(lineseperator);
        });

        try {
            writer = new FileWriter(filename);
            writer.write(sb.toString());

        } catch (IOException ex) {
            Logger.getLogger(PlayList.class.getName()).log(Level.SEVERE,
                    "Kann Playlist Datei nicht schreiben", ex);
            System.out.println("Kann Playlist Datei nicht schreiben");
            throw new RuntimeException(ex);
        } finally {
            try {
                writer.close();
            } catch (Exception ex) {
                // Logger.getLogger(PlayList.class.getName()).log(Level.SEVERE,
                //null, ex);
            }
        }
    }

    public void loadFromM3U(String pathname) {
        Scanner scanner = null;

        try {
            scanner = new Scanner(new File(pathname));
            String line="";
            //only clears List if no Exception happens
            this.clear();
            while(scanner.hasNextLine()){
                line=scanner.nextLine();
                if(!(line.startsWith("#")||line.isEmpty()||
                        Helper.emptyString(line))){
                    try {
                        this.add(AudioFileFactory.getInstance(line));
                    } catch (NotPlayableException ex) {
                        ex.printStackTrace();
                    }
                }
            }
            
            //does a scan per Character m(
//            scanner.forEachRemaining((line)->{
//            });
        } catch (FileNotFoundException ex) {
            System.out.println("Datei nicht gefunden" + pathname);
            Logger.getLogger(PlayList.class.getName()).log(Level.INFO, 
                   "Datei nicht gefunden" + pathname, ex);
            throw new RuntimeException(ex);
        } finally {
            try {
                scanner.close();
            } catch (Exception ex) {
                //empty
            }
        }

    }

    public void sort(SortCriterion sortCriterion) {
        switch (sortCriterion){
            case ALBUM:
                Collections.sort(this, new AlbumComparator());
                break;
            case AUTHOR:
                Collections.sort(this, new AuthorComparator());
                break;
            case TITLE:
                Collections.sort(this, new TitleComparator());
                break;
            case DURATION:
                Collections.sort(this, new DurationComparator());
                break;
            default:
                throw new AssertionError(sortCriterion.name());
        }
    }
}