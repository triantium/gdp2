/*
 * Projekt für das Praktikum GdP2
 * Mediaplayer
  * Erstellt von Manuel Müller
 */

package studiplayer.audio;

/**
 * @author Müller Manuel 
 */
public enum SortCriterion {
    AUTHOR,TITLE,ALBUM,DURATION,
}
