/*
 * Projekt für das Praktikum GdP2
 * Mediaplayer
  * Erstellt von Manuel Müller
 */
package studiplayer.audio;

import java.util.Comparator;

/**
 * @author Müller Manuel
 */
public class DurationComparator implements Comparator<AudioFile> {

    @Override
    public int compare(AudioFile o1, AudioFile o2) {
        long difference;
        difference = o1.getDuration() - o2.getDuration();
        int result;
        // wirklich nur weil es geht ☕
        // und wirklich schwieriger zu debugen ist
//        if (difference == 0) {
//            return 0;
//        } else if (difference < 0) {
//            return -1;
//        } else {
//            return 1;
//        }
        result = (difference == 0) ? 0 : (difference < 0) ? -1 : 1;
        return result;
        //return (int) difference; would also have worked
    }

}
