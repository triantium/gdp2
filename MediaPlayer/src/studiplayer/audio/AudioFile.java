package studiplayer.audio;

/*
 * Projekt für das Praktikum GdP2
 * Mediaplayer
 * Erstellt von Manuel Müller
 */
import java.io.File;
import studiplayer.helper.Helper;

/**
 * @author Manuel Müller
 */
public abstract class AudioFile {

    protected final static String TITLESEPERATOR = " - ";
    private final static char WINSEPERATOR = '\\';
    private final static char UNIXSEPERATOR = '/';

    private String pathName;

    private String fileName;

    protected String author;

    protected String title;

    protected String album;

    protected long duration;

    private char sepChar;

    public abstract void play() throws NotPlayableException;

    public abstract void togglePause();

    public abstract void stop();

    public abstract String getFormattedDuration();

    public abstract String getFormattedPosition();

    public abstract String[] fields();

    public AudioFile(String p) throws NotPlayableException {
        init();
        parsePathname(p);
        parseFilename(getFilename());

        File file = new File(getPathname());
        if (!file.canRead()) {
            throw new NotPlayableException(p, "Cannot read File in Konstruktor"
                    + "for Audiofile");
        }
    }

    public AudioFile() {
        init();
    }

    private void init() {
        pathName = "";
        fileName = "";
        author = "";
        title = "";
        album = "";
        duration = 0;
        sepChar = File.separatorChar;

    }

    public String getPathname() {
        return pathName;
    }

    public String getFilename() {
        return fileName;
    }

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public long getDuration() {
        return duration;
    }

    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        if (author.isEmpty()) {
            res.append(title);
        } else {
            res.append(author);
            res.append(TITLESEPERATOR);
            res.append(title);
        }
        return res.toString();
    }

    public void parsePathname(String pathString) {
        if (Helper.emptyString(pathString)) {
            pathName = pathString;
            fileName = pathString;
            return;
        }
        if (pathString.isEmpty()) {
            pathName = "";
            fileName = "";
            return;
        }
        pathString = normalizePathDescription(pathString);
        pathString = normalizeFileString(pathString);

        pathName = pathString;
        int index = pathString.lastIndexOf(sepChar);
        String fileString;
        if (index < 0) {
            fileString = pathString;
        } else {
            fileString = pathString.substring(index + 1);
        }
        if (fileString.isEmpty()) {
            fileName = "";
        } else {
            fileName = fileString;
//            fileName = fileName.replace("/", "");
        }

    }

    private String normalizePathDescription(String strange) {
        int indexOf = strange.indexOf(':');
        if (!isWindows()) {
            if (indexOf > 0) {
                StringBuilder replaceBuilder = new StringBuilder();
                replaceBuilder.append(sepChar);
                if (indexOf >= 1) {
                    replaceBuilder.append(strange.charAt(indexOf - 1));
                    replaceBuilder.append(sepChar);
                }
                strange = strange.replace(
                        strange.charAt(indexOf - 1) + ":",
                        replaceBuilder.toString());
            }
        }
        return strange;
    }

    private String normalizeFileString(String strange) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < strange.length(); i++) {
            if (strange.charAt(i) == UNIXSEPERATOR
                    || strange.charAt(i) == WINSEPERATOR) {
                if (i >= strange.length() - 1) {
                    result.append(sepChar);
                    break;
                } else if (strange.charAt(i + 1) != UNIXSEPERATOR
                        && strange.charAt(i + 1) != WINSEPERATOR) {
                    result.append(sepChar);
                }
            } else {
                result.append(strange.charAt(i));
            }
        }

        return result.toString();
    }

    private boolean isWindows() {
        return System.getProperty("os.name").toLowerCase().contains("win");
    }

    public void parseFilename(String filename) {
        if (filename.isEmpty()) {
            author = "";
            title = "";
            return;
        }

        String tmpfilename = filename;

        String delim = TITLESEPERATOR;

        int delimIndex = tmpfilename.indexOf(delim);
        if (delimIndex <= 0) {
            author = "";
        } else {
            author = tmpfilename.substring(0, delimIndex);
            author = author.trim();
        }
        delim = ".";
        int delim2Index = tmpfilename.lastIndexOf(delim);
        if (delim2Index >= 0) {
            title = tmpfilename.substring(delimIndex < 0 ? 0 : delimIndex + 3,
                    delim2Index);
        } else {
            title = tmpfilename.substring(delimIndex < 0 ? 0 : delimIndex + 3);
        }
        title = title.trim();

    }

}
