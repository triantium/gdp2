package studiplayer.audio;

import studiplayer.basic.WavParamReader;

/*
 * Projekt für das Praktikum GdP2
 * Mediaplayer
  * Erstellt von Manuel Müller
 */

/**
 * @author Müller Manuel
 */
public class WavFile extends SampledFile {

    public WavFile(String p) throws NotPlayableException {
        super(p);
        readAndSetDurationFromFile(getPathname());
    }

    public static long computeDuration(long frames, float framerate) {
        return (long) ((frames * 1000000) / framerate);
    }

    public void readAndSetDurationFromFile(String path) throws NotPlayableException{
         try {
            WavParamReader.readParams(path);
        } catch (RuntimeException ex) {
            throw new NotPlayableException(getPathname(), ex);
        }
        duration=computeDuration(WavParamReader.getNumberOfFrames(), 
                WavParamReader.getFrameRate());
        
    }
    
    @Override
    public String toString(){
        StringBuilder sb=new StringBuilder();
        sb.append(super.toString());
        sb.append(TITLESEPERATOR);
        sb.append(getFormattedDuration());
        
        return sb.toString();
    }
    
    @Override
    public String[] fields() {
        String field[]={getAuthor(),getTitle(),getAlbum(),
            getFormattedDuration()};
        return field;
    }

}
