/*
 * Projekt für das Praktikum GdP2
 * Mediaplayer
  * Erstellt von Manuel Müller
 */

package studiplayer.audio;

import java.util.Comparator;

/**
 * @author Müller Manuel 
 */
public class TitleComparator implements Comparator<AudioFile>{

    @Override
    public int compare(AudioFile o1, AudioFile o2) {
        if(o1==null||o2==null){
            throw new NullPointerException();
        }else if(o1.getTitle()==null&&o2.getTitle()==null){
            //beide gleich
            return 0;
        }else if(o1.getTitle()==null){
            return -1;
        }else if(o2.getTitle()==null){
            return 1;
        }else{
            return o1.getTitle().compareTo(o2.getTitle());
        }
    }

}
