package studiplayer.audio;



/*
 * Projekt für das Praktikum GdP2
 * Mediaplayer
  * Erstellt von Manuel Müller
 */

/**
 * @author Müller Manuel 
 */
public class AudioFileFactory {

    public static AudioFile getInstance(String pathname) throws NotPlayableException{
        String testPath=pathname.toLowerCase();
        if(testPath.endsWith(".mp3")){
            return new TaggedFile(pathname);
        } else if(testPath.endsWith(".ogg")){
            return new TaggedFile(pathname);
        } else if(testPath.endsWith(".wav")){
            return new WavFile(pathname);
        } else {
//            Optional<AudioFile> file;
//            file=Optional.empty();
//            return file;
            throw new NotPlayableException(pathname, "Unkown or Unimplemented"
                    + " Fileending");
            //
        }
    }

}
