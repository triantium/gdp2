/*
 * Projekt für das Praktikum GdP2
 * Mediaplayer
  * Erstellt von Manuel Müller
 */

package studiplayer.helper;

/**
 * @author Müller Manuel 
 */
public class Helper {
    
        //should normally in a HelperClass, espacilly since it is now public
    //but who knows what the Test does with it
    public static boolean emptyString(String test) {
        boolean res = true;
        for (int i = 0; i < test.length(); i++) {
            if (Character.isLetter(test.charAt(i))) {
                res = false;
                break;
            }
        }
        return res;
    }

}
