/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thi.pruefungsvorbereitung.ss13._3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author manu
 */
public class Stern extends Himmelskoerper {

    List<Planet> planets;

    public Stern(String name, int diameter) {
        super(name, diameter);
        planets = new ArrayList<>();
    }

    public void add(Planet p) {
        planets.add(p);
    }

    public List<Mond> alleMonde() {
        ArrayList<Mond> list = new ArrayList<>();
        for (Planet planet : planets) {
            for (Mond mond : planet.monde) {
                list.add(mond);
            }
        }
        Collections.sort(list);
        return list;

    }

    public void printMonde() {
        alleMonde().forEach(m -> {
            System.out.println(m);
        });
    }
}
