/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thi.pruefungsvorbereitung.ss13;

import java.util.Arrays;

/**
 *
 * @author manu
 */
public class _1 {

    class A {

        protected int x = 3;

        public A() {
            x++;
        }

        public String toString() {
            return "A" + x;
        }
    }

    class B extends A {

        protected int y = 5;

        public B() {
            y = y + x;
        }

        public String toString() {
            return "B" + y;
        }
    }

    class C extends B {

        private int z = 7;

        public C() {
            z = z + y;
        }

        public String toString() {
            return "C" + z;
        }
    }

    private String re(String s) {
        if (s.isEmpty()) {
            return "";
        } else {
            return s.charAt(s.length() - 1) + re(s.substring(0, s.length() - 1));
        }
    }

    public void test() {
        System.out.println("1= " + new A() + "-" + new B() + "-" + new C());
        String[] namen1 = {"Berta", "Doro", "Chris", "Adam"}, namen2 = namen1;
        Arrays.sort(namen2); // wie Collections.sort( ), aber fuer Arrays
        System.out.println("2= " + namen1[0] + ", " + namen2[3]);
        String s = "Mississippi", t = s.substring(0, s.lastIndexOf('s'));
        System.out.println("3= " + t + " " + re(t.substring(1)));
        System.out.println("4= " + 2 + 3);
        System.out.println("5= " + (3 + 2));
        System.out.println(2 + 3 + "Ende");

    }
}
