/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thi.pruefungsvorbereitung.ss13._3;

/**
 *
 * @author manu
 */
public class Mond extends Himmelskoerper {

    public Mond(String name, int diameter) {
        super(name, diameter);
    }

    @Override
    public String toString() {
        return "Mond{" + "name=" + name + ", diameter=" + diameter + '}';
    }

}
