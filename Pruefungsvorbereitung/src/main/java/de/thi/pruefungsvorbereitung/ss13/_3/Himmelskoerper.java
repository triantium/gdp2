/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thi.pruefungsvorbereitung.ss13._3;

/**
 *
 * @author manu
 */
public abstract class Himmelskoerper implements Comparable<Himmelskoerper> {

    protected String name;
    protected int diameter;

    public Himmelskoerper(String name, int diameter) {
        this.name = name;
        this.diameter = diameter;
    }

    public String getName() {
        return name;
    }

    public int getDiameter() {
        return diameter;
    }

    @Override
    public int compareTo(Himmelskoerper o) {
        return (o.diameter - this.diameter);
    }

    @Override
    public String toString() {
        return "Himmelskoerper{" + "name=" + name + ", diameter=" + diameter + '}';
    }

}
