/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thi.pruefungsvorbereitung.ss13;

/**
 *
 * @author manu
 */
public class Polynom {

    int[] arr;

    public Polynom(int[] arr) {
        this.arr = arr;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (arr[0] < 0) {
            sb.append("- ");
        }
        for (int i = 0; i < arr.length; i++) {
            int r = arr[i];
            int exp = arr.length - i - 1;
            if (r != 0) {
                sb.append(getTerm(r, exp));
            }
            if (i < arr.length - 1 && arr[i + 1] != 0) {
                sb.append(arr[i + 1] > 0 ? " + " : " - ");
            }
        }
        return sb.toString();
    }

    private String getTerm(int a, int exp) {
        StringBuilder sb = new StringBuilder();
        a = Math.abs(a);
        switch (exp) {
            case 0:
                sb.append(Integer.toString(a));
                break;
            case 1:
                sb.append(Integer.toString(a));
                sb.append("x");
                break;
            default:
                sb.append(Integer.toString(a));
                sb.append("x^");
                sb.append(Integer.toString(exp));
                break;

        }
        return sb.toString();
    }

}
