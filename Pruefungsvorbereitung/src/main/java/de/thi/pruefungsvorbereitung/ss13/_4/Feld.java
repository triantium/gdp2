/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thi.pruefungsvorbereitung.ss13._4;

import javax.swing.JButton;

/**
 *
 * @author manu
 */
public class Feld extends JButton {

    private int zeile, spalte;

    public Feld(int z, int s, int zahl) {
        zeile = z;
        spalte = s;
        if (zahl == 0) {
            setText("");
        } else {
            setText("" + zahl);
        }
    }

    public int getZ() {
        return zeile;
    }

    public int getS() {
        return spalte;
    }
}
