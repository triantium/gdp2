/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thi.pruefungsvorbereitung.ss13._4;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author manu
 */
public class Schiebepuzzle implements ActionListener {

    private Feld frei; // hier halten wir eine Referenz auf das AKTUELL freie Feld
// das nutzt uns spaeter, weil wir es nicht lange suchen muessen

    public Schiebepuzzle(int[][] mat) {
        JFrame fr = new JFrame("Schiebepuzzle");
        JPanel pn = new JPanel(new GridLayout(mat.length, mat[0].length));
        fr.add(pn);
        for (int r = 0; r < mat.length; r++) {
            for (int s = 0; s < mat[r].length; s++) {
                Feld feld = new Feld(r, s, mat[r][s]);
                feld.setBackground(Color.LIGHT_GRAY);
                pn.add(feld);
                feld.addActionListener(this);
                if (mat[r][s] == 0) {
                    frei = feld;
                }
            }
        }
        frei.setBackground(Color.WHITE);
        fr.pack();
        fr.setVisible(true);
    } // end Schiebepuzzle

    public void actionPerformed(ActionEvent e) {
        Feld f = (Feld) e.getSource();
        System.out.println(f.getZ() + " " + f.getS());
        if (Math.abs(f.getS() - frei.getS()) == 1 && Math.abs(f.getZ() - frei.getZ()) == 0) {
            frei.setBackground(Color.LIGHT_GRAY);
            f.setBackground(Color.WHITE);

            frei.setText(f.getText());
            f.setText("");

            frei = f;
        } else if (Math.abs(f.getS() - frei.getS()) == 0 && Math.abs(f.getZ() - frei.getZ()) == 1) {

            frei.setBackground(Color.LIGHT_GRAY);
            f.setBackground(Color.WHITE);

            frei.setText(f.getText());
            f.setText("");

            frei = f;

        } else {
            f.setBackground(Color.LIGHT_GRAY);
            System.out.println("not a Neighbour");
        }
    } // end actionPerformed

}
