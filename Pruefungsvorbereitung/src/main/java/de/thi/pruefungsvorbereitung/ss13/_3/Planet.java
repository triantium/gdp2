/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thi.pruefungsvorbereitung.ss13._3;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author manu
 */
public class Planet extends Himmelskoerper {

    public List<Mond> monde;

    public Planet(String name, int diameter) {
        super(name, diameter);
        monde = new ArrayList<>();
    }

    public void add(Mond mond) {
        monde.add(mond);
    }

}
