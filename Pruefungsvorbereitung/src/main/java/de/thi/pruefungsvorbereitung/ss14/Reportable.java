/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thi.pruefungsvorbereitung.ss14;

import java.util.List;
import java.util.Map;

/**
 *
 * @author manu
 */
public interface Reportable {

    String getHeader();

    List<String> getLegend();

    Map<String, List<Integer>> getLines();
}
