/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thi.pruefungsvorbereitung.ss14;

import java.util.List;

/**
 *
 * @author manu
 */
public class ReportGenerator {

    public void testMe() {
        Statistics roboStat = new Statistics(
                "Zahl der Industrieroboter in Tausend",
                new String[]{"Land", "2010", "2011", "2012"});
        roboStat.add("Japan", new int[]{308, 307, 311});
        roboStat.add("USA", new int[]{180, 193, 207});
        roboStat.add("D",
                new int[]{148, 157, 162});
        printReport(roboStat);
    }

    private void printReport(Reportable r) {
        System.out.println(r.getHeader());
        for (String p : r.getLegend()) {
            System.out.print(p + "\t");
        }
        System.out.println();
        for (String k : r.getLines().keySet()) {
            List<Integer> v = r.getLines().get(k);
            System.out.print(k + "\t");
            for (int i : v) {
                System.out.print(i + "\t");
            }
            System.out.println();
        }
    }

}
