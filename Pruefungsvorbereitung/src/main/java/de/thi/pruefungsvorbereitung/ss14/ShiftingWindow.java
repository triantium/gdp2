package de.thi.pruefungsvorbereitung.ss14;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *  //SS2014
 *
 * @author manu
 */
public class ShiftingWindow extends Frame implements ActionListener {

    public ShiftingWindow() {
        super("Shift-A-Window");
        Panel tp = new Panel(new BorderLayout());
        Panel gp = new Panel(new GridLayout(3, 3));
        add(tp);
        tp.add(gp, BorderLayout.CENTER);
        tp.add(new Label("press"), BorderLayout.NORTH);
        Button exit = new Button("exit");
        exit.addActionListener(this);
        tp.add(exit, BorderLayout.SOUTH);
        Button b1 = new Button("^");
        b1.addActionListener(this);
        Button b2 = new Button("v");
        b2.addActionListener(this);
        Button b3 = new Button("<");
        b3.addActionListener(this);
        Button b4 = new Button(">");
        b4.addActionListener(this);

        gp.add(new Label());
        gp.add(b1);
        gp.add(new Label());
        gp.add(b3);
        gp.add(new Label());
        gp.add(b4);
        gp.add(new Label());
        gp.add(b2);

        pack();
        setVisible(true);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String cmd = e.getActionCommand();
        if (cmd.equals("exit")) {
            System.exit(0);
        } else {
            int x = getLocation().x, y = getLocation().y;
            if (cmd.equals("^")) {
                y -= 20;
            } else if (cmd.equals("v")) {
                y += 20;
            } else if (cmd.equals("<")) {
                x -= 20;
            } else if (cmd.equals(">")) {
                x += 20;
            }
            setLocation(x, y);
        }
    }

}
