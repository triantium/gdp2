/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thi.pruefungsvorbereitung.ss14;

/**
 *
 * @author manu
 */
public class BioInformatik {

    public static String spleissen(String work, String be, String en) {
        while (work.contains(be)) {
            int start = work.indexOf(be);
            int ende = work.indexOf(en, start) + en.length();
            String begin = work.substring(0, start);
            String end = work.substring(ende, work.length());
            work = begin + end;
        }
        return work;
    }

}
