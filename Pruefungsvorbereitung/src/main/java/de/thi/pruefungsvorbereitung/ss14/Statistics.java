/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thi.pruefungsvorbereitung.ss14;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author manu
 */
public class Statistics implements Reportable {

    String header;
    ArrayList<String> legend;
    HashMap<String, List<Integer>> lines;

    Statistics(String h, String[] string) {
        lines = new HashMap<>();
        header = h;
        legend = new ArrayList<>(Arrays.asList(string));
    }

    public String getHeader() {
        return header;
    }

    public ArrayList<String> getLegend() {
        return legend;
    }

    public HashMap<String, List<Integer>> getLines() {
        return lines;
    }

    void add(String key, int[] i) {
        ArrayList<Integer> list = new ArrayList();
        for (Integer iv : i) {
            list.add(iv);
        }
        lines.put(key, list);
    }

}
