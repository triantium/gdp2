/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thi.pruefungsvorbereitung.ss07;

/**
 *
 * @author manu
 */
public class StupidCar {

    class Auto {

        Auto() {
            System.out.println("Auto");
        }

        Auto(String name) {
            this();
            System.out.println("Auto mit Name");
        }

        public String toString() {
            return "Automobil";
        }
    }

    class Pkw extends Auto {

        Pkw() {
            this("unbekannt");
            System.out.println("Pkw");
        }

        Pkw(String name) {
            System.out.println(this + " mit Name");
        }
    }

    class Cabrio extends Pkw {

        Cabrio(int sitze) {
            System.out.println("Cabrio");
        }
    }

    public void testMe() {
        System.out.println(new Cabrio(2));
    }

}
