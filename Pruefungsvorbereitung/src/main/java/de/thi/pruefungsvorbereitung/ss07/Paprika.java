/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thi.pruefungsvorbereitung.ss07;

import java.util.Arrays;

/**
 *
 * @author manu
 */
public class Paprika implements Comparable<Paprika> {

    enum Farbton {
        GRÜN, GELB, ORANGE, ROT
    };

    private Farbton farbe;

    Paprika(Farbton farbe) {
        this.farbe = farbe;
    }

    public int compareTo(Paprika p) {
        return farbe.ordinal() - p.farbe.ordinal();
    }

    public String toString() {
        return "Paprika " + farbe;
    }

    public static void toWrite() {
        Paprika[] arr = {new Paprika(Farbton.GELB), new Paprika(Farbton.ROT),
            new Paprika(Farbton.ORANGE), new Paprika(Farbton.GRÜN), new Paprika(Farbton.GRÜN), new Paprika(Farbton.GRÜN), new Paprika(Farbton.GRÜN),
             new Paprika(Farbton.GRÜN)};
        Arrays.sort(arr);
        for (Paprika pap : arr) {
            System.out.println(pap);
        }

    }
}
