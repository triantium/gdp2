/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thi.pruefungsvorbereitung.ss07;

/**
 *
 * @author manu
 */
public class C1 {

    interface X {

        public String x();
    }

    abstract class Y implements X {

        public String toString() {
            return "Y";
        }
    }

    class Z extends Y {

        public String x() {
            return "X";
        }

        public String toString() {
            return "Z";
        }
    }

    class Programm {

        void f(X x) {
            System.out.println(x);
        }

        void g(Y y) {
            System.out.println(y);
        }

        void h(Z z) {
            System.out.println(z);
        }

        public void print() {
            f(new Z());
            g(new Z());
            h(new Z());
        }
    }

    public void testMe() {
        new Programm().print();
    }
}
