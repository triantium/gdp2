/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thi.pruefungsvorbereitung.ss07;

/**
 *
 * @author manu
 */
public class B1 {

    class A {

        private int i;

        A(int zahl) {
            i = zahl;
        }

        int get() {
            return i;
        }
    }

    public void testMe() {
        A a1 = new A(47);
        A a2 = new A(11);
        System.out.println(a1.get());
        System.out.println(a2.get());
        System.out.println(a1.get() + a2.get());
    }

}
