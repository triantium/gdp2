/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thi.pruefungsvorbereitung.ss12;

/**
 *
 * @author Manuel Müller
 */
public class Wein implements Comparable<Wein> {

    private String name;
    private double preis;

    public Wein(String name, double preis) {
        this.name = name;
        this.preis = preis;
    }

    @Override
    public int compareTo(Wein o) {
        if (this.preis == o.preis) {
            return (this.name.compareTo(o.name));
        } else if (o.preis > this.preis) {
            return 1;
        } else {
            return -1;
        }
    }

    @Override
    public String toString() {
        return "Wein{" + "name=" + name + ", preis=" + preis + '}';
    }

}
