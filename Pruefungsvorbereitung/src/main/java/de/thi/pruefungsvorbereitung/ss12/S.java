/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thi.pruefungsvorbereitung.ss12;

/**
 *
 * @author Manuel Müller
 */
public class S extends C {

    public S(String who) {
        this();
        if (!tell) {
            System.out.println("alias David John Moore Cornwell");
            System.out.println(who);
        } else {
            System.out.println("alias John Le Carre");
        }
    }

    public S() {
        System.out.println(this);
    }

    public String toString() {
        if (!tell) {
            return whoAreYou();
        } else {
            System.out.println("Spy");
            return "by David John Moore Cornwell";
        }
    }
}
