/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thi.pruefungsvorbereitung.ss12;

/**
 *
 * @author Manuel Müller
 */
public class C extends T {

    public C() {
        System.out.println(this);
        System.out.println("Soldier");
        tell = true;
    }

    public String toString() {
        System.out.println("von John Le Carre");
        return "Spion";
    }
}
