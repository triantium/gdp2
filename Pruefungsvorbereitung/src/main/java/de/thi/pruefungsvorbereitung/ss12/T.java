/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thi.pruefungsvorbereitung.ss12;

/**
 *
 * @author Manuel Müller
 */
public class T {

    protected boolean tell = false;

    public T() {
        if (!tell) {
            System.out.println("Tinker");
        } else {
            System.out.println("Dame");
        }
    }

    public String whoAreYou() {
        return "Tailor";
    }

    public String toString() {
        return "Koenig";
    }
}
