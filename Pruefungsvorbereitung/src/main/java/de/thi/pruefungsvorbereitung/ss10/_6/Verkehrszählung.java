/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thi.pruefungsvorbereitung.ss10._6;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author manu
 */
public class Verkehrszählung extends JFrame implements ActionListener {

    private int pkw = 0, lkw = 0;
    private JLabel anzahlLabel = new JLabel("Anzahl ");
    private JPanel centerPanel = new JPanel();
    private JButton lkwButton = new JButton("LKW");
    private JButton pkwButton = new JButton("PKW");
    private JTextField lkwTextField = new JTextField("0");
    private JTextField pkwTextField = new JTextField("0 ");
    private JButton resetButton = new JButton("Reset");

    public Verkehrszählung() {
        super("VZ");

        //a)
        resetButton.addActionListener(this);
        lkwButton.addActionListener(this);
        pkwButton.addActionListener(this);

        JPanel g1 = new JPanel(new BorderLayout());
        JPanel g2 = new JPanel(new GridLayout(2, 2));
        this.add(centerPanel);
        centerPanel.add(g1);
        g1.add(anzahlLabel, BorderLayout.NORTH);
        g1.add(g2, BorderLayout.CENTER);
        g1.add(resetButton, BorderLayout.SOUTH);
        g2.add(lkwTextField);
        g2.add(pkwTextField);
        g2.add(lkwButton);
        g2.add(pkwButton);

        pack();
        setVisible(true);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String cmd = e.getActionCommand().toLowerCase();
        if (cmd.equals("reset")) {
            lkw = 0;
            pkw = 0;
        } else if (cmd.equals("lkw")) {
            lkw++;
        } else if (cmd.equals("pkw")) {
            pkw++;
        }
        lkwTextField.setText(Integer.toString(lkw));
        pkwTextField.setText(Integer.toString(pkw));
    }

}
