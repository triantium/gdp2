/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thi.pruefungsvorbereitung.ss10._2;

/**
 *
 * @author manu
 */
public class UseVektor2D {

    public void testMe() {
        Vektor2D v1 = new Vektor2D(5.0, 7.0);
        Vektor2D v2 = new Vektor2D(7.0, 2.0);

        System.out.println("Ausgabe von v1: " + v1);
        System.out.println("Ausgabe von v2: " + v2);

        Vektor2D v3 = v1.add(5.0);
        System.out.println("5 + v1 ergibt: " + v3);

        Vektor2D v4 = v1.add(v2);
        System.out.println("v1 + v2 ergibt : " + v4
        );
    }
}
