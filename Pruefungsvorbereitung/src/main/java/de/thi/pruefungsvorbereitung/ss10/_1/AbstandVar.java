/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thi.pruefungsvorbereitung.ss10._1;

/**
 *
 * @author manu
 */
public class AbstandVar {

    class Abstand {

        private double abstand;

        Abstand(double a) {
            abstand = a;
            System.out.println("Abstand " + a + " erzeugt! ");
        }

        Abstand(double x1, double y1, double x2, double y2) {
            this(Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1)));
            System.out.println("Abstand berechnet! ");
        }

        void setAbstand(double a) {
            abstand = a;
        }

        public String toString() {
            return "" + abstand + " ";
        }
    }

    public void testMe() {
        int x, y = 42;
        String r = "Sommer", s = " Winterreifen", t;
        Abstand d1, d2, d3;
        x = y;
        y = 2 * x;
        x++;
        System.out.println(x + " , " + y);
        t = s;
        s = r + "traum ";
        System.out.println(t + " , " + s);
        d1 = new Abstand(1.0);
        d2 = new Abstand(0, 0, 2, 0);
        d3 = d1;
        d1.setAbstand(2.0);
        System.out.println(" " + d1 + d2 + d3);
        System.out.println((d1 == d2) + " , " + (d1 == d3));
    }

}
