/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thi.pruefungsvorbereitung.ss10._5;

/**
 *
 * @author manu
 */
public class Spieler {

    private String name;
    private int tore = 0;

    Spieler(String n) {
        name = n;
    }

    void addTor() {
        tore++;
    }

    int getTore() {
        return tore;
    }
}
