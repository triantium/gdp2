/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thi.pruefungsvorbereitung.ss10._4;

/**
 *
 * @author manu
 */
public class Reihung {

    private String[] reihung = new String[10];

    void insert(int index, String element) throws Exception {

        try {
            reihung[index] = element;
        } catch (IndexOutOfBoundsException ex) {
            if (index < 0) {
                throw new Exception();
            };
            String[] nR = new String[index + 1];
            for (int i = 0; i < reihung.length; i++) {
                nR[i] = reihung[i];
            }
            nR[index] = element;
            reihung = nR;
        }
    }

}
