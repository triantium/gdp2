/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thi.pruefungsvorbereitung.ss10._3;

/**
 *
 * @author manu
 */
public class Gemüse implements Ware {

    @Override
    public double getPreis() {
        return 2.0;
    }

    @Override
    public String getWarenName() {
        return "Gemüse";
    }

    @Override
    public void printPreisListe(Ware[] w) {

    }

    @Override
    public double getGewicht() {
        return 6.0;
    }

}
