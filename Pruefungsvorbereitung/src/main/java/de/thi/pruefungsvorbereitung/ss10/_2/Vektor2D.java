/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thi.pruefungsvorbereitung.ss10._2;

/**
 *
 * @author manu
 */
public class Vektor2D {

    double x, y;

    Vektor2D(double d, double d0) {
        x = d;
        y = d0;
    }

    Vektor2D add(double d) {
        return new Vektor2D(x + d, y + d);
    }

    Vektor2D add(Vektor2D v) {
        return new Vektor2D(this.x + v.x, this.y + v.y);
    }

    public String toString() {
        return "(" + x + "," + y + ")";
    }

}
