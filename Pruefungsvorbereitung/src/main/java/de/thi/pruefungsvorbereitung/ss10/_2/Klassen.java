/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thi.pruefungsvorbereitung.ss10._2;

/**
 *
 * @author manu
 */
public class Klassen {

    class A {

        int a;
    }

    class B extends A {

        int b;
    }

    class C extends B {

        int c;
    }

    public void testMe() {
        A a = new A();
        B b = new B();
        C c = new C();

        //Zulässig
        a = c;
        b.a = a.a;

        //Unzulässig
        //b = a;
    }

}
