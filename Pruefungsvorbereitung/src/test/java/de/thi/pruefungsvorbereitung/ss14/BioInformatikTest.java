/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thi.pruefungsvorbereitung.ss14;

import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author manu
 */
public class BioInformatikTest {

    public BioInformatikTest() {
    }

    /**
     * Test of spleissen method, of class BioInformatik.
     */
    @Test
    public void testSpleissen() {
        String test = "AUAGUAAAAGCUCUGUUUAGGAGA";
        String begin = "GU";
        String end = "AG";
        String expected = "AUACUCUGAGA";
        String result = BioInformatik.spleissen(test, begin, end);
        System.out.println(result);
        Assert.assertEquals(expected, result);
    }

}
