/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thi.pruefungsvorbereitung.ss13._3;

import org.junit.Test;

/**
 *
 * @author manu
 */
public class SternTest {

    public SternTest() {
    }

    /**
     * Test of add method, of class Stern.
     */
    /**
     * Test of printMonde method, of class Stern.
     */
    @Test
    public void testPrintMonde() {
        Stern sonne = new Stern("Sonne", 1390000);
        Planet merkur = new Planet("Merkur", 4880);
        Planet venus = new Planet("Venus", 12100); // usw. Erde, Mars etc
        Planet erde = new Planet("Erde", 4880);
        Planet mars = new Planet("Erde", 4880);
        sonne.add(merkur);
        sonne.add(venus); // usw.
        sonne.add(erde);
        sonne.add(mars);
// und noch die Monde:
        erde.add(new Mond("Mond", 3480));
        mars.add(new Mond("Phobos", 27));
        mars.add(new Mond("Deimos", 15));
        sonne.printMonde();
    }

}
