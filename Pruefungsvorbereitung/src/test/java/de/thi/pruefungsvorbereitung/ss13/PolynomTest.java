/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thi.pruefungsvorbereitung.ss13;

import org.junit.Test;

/**
 *
 * @author manu
 */
public class PolynomTest {

    public PolynomTest() {
    }

    /**
     * Test of toString method, of class Polynom.
     */
    @Test
    public void testToString() {
        System.out.println(new Polynom(new int[]{4, 0, -2, 7}));
        System.out.println(new Polynom(new int[]{-3, 2, 0}));
        System.out.println(new Polynom(new int[]{1, -1, 1, -1}));
        System.out.println(new Polynom(new int[]{-2, 5, 0, 3}));

    }

}
