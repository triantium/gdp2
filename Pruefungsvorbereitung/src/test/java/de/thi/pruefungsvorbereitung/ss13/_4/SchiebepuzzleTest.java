/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thi.pruefungsvorbereitung.ss13._4;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;

/**
 *
 * @author manu
 */
public class SchiebepuzzleTest {

    public SchiebepuzzleTest() {
    }

    /**
     * Test of actionPerformed method, of class Schiebepuzzle.
     */
    @Test
    public void testMe() {
        new Schiebepuzzle(new int[][]{
            {11, 5, 12, 14},
            {15, 2, 0, 9},
            {13, 7, 6, 1},
            {3, 10, 4, 8}
        });

        try {
            Thread.sleep(60000);
        } catch (InterruptedException ex) {
            Logger.getLogger(SchiebepuzzleTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
