/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ss_20cds;

import de.thi.pruefungsvorbereitung.ss12.Wein;
import java.util.ArrayList;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Manuel Müller
 */
public class WeinTest {

    public WeinTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of compareTo method, of class Wein.
     */
    @Test
    public void testCompareTo() {
        System.out.println("compareTo");
        ArrayList<Wein> list = new ArrayList();

        list.add(new Wein("A", 1.00));
        list.add(new Wein("Z", 1.00));
        list.add(new Wein("v", 5844.00));
        list.add(new Wein("z", 550.00));
        list.add(new Wein("B", 550.00));
        list.add(new Wein("C", 550.00));
        list.add(new Wein("A", 5550.00));
        list.sort(null);
        list.forEach((t) -> {
            System.out.println(t.toString());
        });
        // TODO review the generated test code and remove the default call to fail.
    }

}
