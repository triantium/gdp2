/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thi.layout;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author manu
 */
public class LayoutManagment {

    /*Positionierung von GUI Elementen ist möglich .. über
    a) über setLocation(x,y)+setSize(w,h)
        aufwändig + fehleranfällig + änderungsunfreundlich

    b) über "Regeln", die dann ein LayoutManager beim Platzieren anwendet
    hier: FlowLayout, GridLayout, BorderLayout

    b1) Flowlayout:= Regel = Komponenten nebeneinander der Reihe nach

    |buttton1||label1|...||end|

    b2) GridLayout (Zeilenzahl, Spaltenzahl)


    BSB GridLayout (3,2)
                    _______________
                   |______|________|
                   |______|________|
                   |______|________|
    Inhalt wird aufgeblasen nach Größten Element

    b3) BorderLayout
    siehe Primefaces Layout


    _________________________________________
    |   <-    North     ->                  |
    -----------------------------------------
    |       |                        |      |
    |       |                        |      |
    | East  |                        | West |
    |       |                        |      |
    |       |                        |      |
    -----------------------------------------
    |           <-South->                   |
    -----------------------------------------

    setLayout(new Borderlayout());
    add(new Label("Zentrum)",BorderLayout.CENTER);
    add(new Button("North"),BorderLayout.NORTH);
    .
    .
    .
    pack();

    c) Schachtelung von LayoutManagern: damit kann man komplexere Öberflachen erstelln,
    die Portabel sind
    Beispiel:

    ----------------
    | b1    |   b4  |
    ----------------
    |   b2  |b5|b6|b7|
    -----------------
    | b3    |   b8  |
    ------------------

    Frame mit Grid(1,2)

    Panel mit Grid(3,1)                     Panel mit BorderLayout

    Button 1    Button 2    Button 3        B4(n).....

    oder Darstellung als Venn-Diagramm

     */
    public void makeWindow() {

        JFrame frame = new JFrame();
        frame.setLayout(new GridLayout(1, 2));
        JPanel panelleft = new JPanel(new GridLayout(3, 1));
        JPanel pannelright = new JPanel(new BorderLayout());
        frame.add(panelleft);
        frame.add(pannelright);
        panelleft.add(new JButton("B1"));
        panelleft.add(new JButton("B2"));
        panelleft.add(new JButton("B2"));

        pannelright.add(new JButton("B4"), BorderLayout.NORTH);
        pannelright.add(new JButton("B5"), BorderLayout.WEST);
        pannelright.add(new JButton("B6"), BorderLayout.CENTER);
        pannelright.add(new JButton("B7"), BorderLayout.EAST);
        pannelright.add(new JButton("B8"), BorderLayout.SOUTH);
        frame.pack();
        frame.setVisible(true);
    }

    public void makeSecondWindow() {

        JFrame frame = new JFrame();
        frame.setLayout(new GridLayout(1, 2));
        JPanel panelleft = new JPanel(new GridLayout(3, 1));
        JPanel pannelright = new JPanel(new BorderLayout());
        JPanel bPanel = new JPanel(new FlowLayout());
        frame.add(panelleft);
        frame.add(pannelright);
        panelleft.add(new JButton("B1"));
        panelleft.add(new JButton("B2"));
        panelleft.add(new JButton("B2"));

        pannelright.add(new JButton("B4"), BorderLayout.NORTH);

        pannelright.add(bPanel);
        bPanel.add(new JButton("B5"));
        bPanel.add(new JButton("B6"));
        bPanel.add(new JButton("B7"));

        pannelright.add(new JButton("B8"), BorderLayout.SOUTH);
        frame.pack();
        frame.setVisible(true);
    }

    public void makeThirdWindow() {

        JFrame frame = new JFrame();
        frame.setLayout(new GridLayout(3, 2));

        frame.add(new JButton("B1"));
        frame.add(new JButton("B4"));
        frame.add(new JButton("B2"));

        JPanel bPanel = new JPanel(new GridLayout(1, 3));
        bPanel.add(new JButton("B5"));
        bPanel.add(new JButton("B6"));
        bPanel.add(new JButton("B7"));
        frame.add(bPanel);
        frame.add(new JButton("B3"));
        frame.add(new JButton("B8"));
        frame.pack();
        frame.setVisible(true);
    }
}
