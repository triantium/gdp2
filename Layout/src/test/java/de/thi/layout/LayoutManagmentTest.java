/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.thi.layout;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;

/**
 *
 * @author manu
 */
public class LayoutManagmentTest {

    public LayoutManagmentTest() {
    }

    /**
     * Test of makeWindow method, of class LayoutManagment.
     */
    @Test
    public void testMakeWindow() {
        new LayoutManagment().makeWindow();
        new LayoutManagment().makeSecondWindow();
        new LayoutManagment().makeThirdWindow();
        try {
            Thread.sleep(60000);
        } catch (InterruptedException ex) {
            Logger.getLogger(LayoutManagmentTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
